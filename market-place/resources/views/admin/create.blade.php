@extends('admin.template')

@section('content')
@if ($message = Session::get('success'))
<div class="alert alert-success mt-3" role="alert">
    <strong>{{ $message }}</strong>
</div>
@endif
<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
    <h1 class="h2">Add Products</h1>
</div>
<form action="/admin/store" method="POST" enctype="multipart/form-data">
    @csrf
    <div class="mb-3">
        <label for="nama_produk" class="form-label">nama produk</label>
        <input type="text" class="form-control" id="nama_produk" name="nama_produk">
    </div>
    <div class="row mb-3">
        <div class="col">
            <label for="stock_produk" class="form-label">stock produk</label>
            <input type="text" class="form-control" id="stock_produk" name="stock_produk">
        </div>
        <div class="col">
            <label for="berat_produk" class="form-label">Berat produk</label>
            <input type="text" class="form-control" id="berat_produk" name="berat_produk">
        </div>
    </div>
    <div class="mb-3">
        <label for="harga_produk" class="form-label">harga produk</label>
        <input type="text" class="form-control" id="harga_produk" name="harga_produk">
    </div>
    <div class="mb-3">
        <label for="deskripsi" class="form-label">Deskripsi</label>
        <input type="text" class="form-control" id="deskripsi" name="deskripsi">
    </div>
    <div class="mb-3">
        <label for="gambar_produk" class="form-label">gambar produk</label>
        <input type="file" class="form-control" id="gambar_produk" name="gambar_produk">
    </div>
    <div class="mb-3">
        <button type="submit" class="form-control btn btn-primary">Submit</button>
    </div>
</form>
@endsection