@extends('admin.template')

@section('content')
<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
    <h1 class="h2">Products</h1>
    <div class="btn-toolbar mb-2 mb-md-0">
      <a href="/admin/create" class="btn btn-sm btn-outline-secondary">
        Add Products
      </a>
    </div>
  </div>
<table class="table">
    <thead>
      <tr>
        <th scope="col">No</th>
        <th scope="col">Nama Produk</th>
        <th scope="col">Stock</th>
        <th scope="col">Berat (satu ikan)</th>
        <th scope="col">Harga Produk</th>
        <th scope="col">deskripsi</th>
        <th scope="col">Gambar Produk</th>
        <th scope="col">Aksi</th>

      </tr>
    </thead>
    <tbody>
      @foreach ($datas as $d)
      <?php
      $string = $d->detail->gambar;
      $pecah = explode('\\', $string);
      $img = end($pecah);
      // dd($img);  
      ?>
      <tr>
        <th scope="row">{{$d->id}}</th>
        <td>{{ $d->nama_produk }}</td>
        <td>{{ $d->stock }}</td>
        <td>{{ $d->berat }} kg</td>
        <td>{{ $d->detail->harga_produk }}</td>
        <td>{{ $d->detail->deskripsi }}</td>
        <td>
          <img src="../../image/{{ $img }}" alt="" style="width: 9rem;">
        </td>
        <td>
          <a href="/admin/edit/{{ $d->id }}" class="btn btn-primary">Update</a>
          <a href="/admin/delete/{{ $d->id }}" class="btn btn-danger">Delete</a>
        </td>
      </tr>
      @endforeach
    </tbody>
  </table>
@endsection