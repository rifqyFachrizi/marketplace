@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        {{-- <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Dashboard') }}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    {{ __('You are logged in!') }}
                </div>
            </div>
        </div> --}}
        @foreach ($produk as $item)
        <?php
        // dd($item->detail->gambar);
        $img = $item->detail->gambar;
        $pecah = explode('\\', $img);
        // dd($pecah);
        $image = $pecah[3];
        ?>
        <div class="card mr-4" style="width: 18rem;">
            <img src="image/{{ $image }}" class="card-img-top" alt="...">
            <div class="card-body">
                <h5 class="card-title">{{ $item->nama_produk }}</h5>
                <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                <a href="/order/detail/{{ $item->id }}" class="btn btn-primary">order</a>
            </div>
        </div>
        @endforeach
    </div>
</div>
@endsection
