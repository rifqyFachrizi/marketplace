@extends('layouts.app')

@section('content')
<div class="container">
    <div class="card mr-4" style="width: 18rem;">
        <img src="../../image/{{ $img }}" class="card-img-top" alt="...">
        <div class="card-body">
            <h5 class="card-title">{{ $data->nama_produk }}</h5>
            <h5 class="card-text">{{ $data->detail->harga_produk }}</h5>
            <h5 class="card-text">{{ $data->stock }}</h5>
            <h5 class="card-text">{{ $data->berat }} kg/ikan</h5>
            <p class="card-text">berisi deskripsi</p>
            <a href="/order/pay/{{ $data->id }}" class="btn btn-light">Beli langsung</a> {{-- lngsung nota --}}
            <a href="/order/cart/store/{{ $data->id }}" class="btn btn-primary">Tambah Keranjang!</a> {{-- keranjang dlu --}}
        </div>
    </div>
</div>
@endsection