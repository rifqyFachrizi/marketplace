@php
    use Illuminate\Support\Facades\DB;
    use App\Models\Produk;
@endphp

@extends('layouts.app')

@section('content')
<div class="container">
    <form action="">
        
    </form>
    <table class="table">
        <thead>
            <tr>
        <th scope="col">No</th>
        <th scope="col">produk</th>
        <th scope="col">berat</th>
        <th scope="col">jumlah</th>
        <th scope="col">harga</th>
        <th scope="col">berat total</th>
        <th scope="col">harga total</th>
        <th scope="col">aksi</th>
    </tr>
</thead>
<tbody>
    @php    
        $i=1;

        // $arr = array_merge($datas,$jml);
        // dd($arr);

        $total_belanja = 0;
        $total_berat = 0;
        @endphp
        @foreach ($count as $item)
        @php
            $id_produk = $item->produk_id;
            // dump($item);
            $produk = Produk::find($id_produk);
            // dump($produk);
            $jmlh = $produk->detail->harga_produk*$item->jumlah;
            $total_belanja += $jmlh;
            $berat = $produk->berat*$item->jumlah;
            $total_berat += $berat;
            
            

        @endphp
        <tr>
            <th scope="row">{{ $i }}</th>
            <td>{{ $produk->nama_produk }}</td>
            <td>{{ $produk->berat }}</td>
            <td>{{ $item->jumlah }}</td>
            <td>Rp {{ number_format($produk->detail->harga_produk) }}</td>
            <td>{{ $berat }} Kg</td>
            <td>Rp {{ number_format($jmlh) }}</td>
            <td>
                <a href="/order/cart/store/{{ $produk->id }}" class="btn btn-success">+</a>
                <a href="/order/cart/minus/{{ $produk->id }}" class="btn btn-danger">-</a>
                {{-- sampe mengatur + - untuk cart --}}
            </td>
        </tr>
        @php
            $i++;
        @endphp
    @endforeach
    </tbody>
    <tfoot>
        <tr>
            <th colspan="5">Total Belanja</th>
            <th>{{ $total_berat }} Kg</th>
            <th>Rp {{ number_format($total_belanja) }}</th>
            <th>
                <a href="/home" class="btn btn-secondary">Back</a>
                <a href="/order/invoice/{{ $id_user }}" class="btn btn-primary">Checkout</a>
            </th>
        </tr>
    </tfoot>
</table>
</div>
@endsection