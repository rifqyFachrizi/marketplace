@extends('layouts.app')

@section('content')
@php
    // dd($data);
@endphp
    <div class="container">
        <div class="mb-3">
            <label for="name" class="form-label">Username</label>
            <input type="text" class="form-control" id="name" placeholder="{{ $user->name }}">
        </div>
        <div class="mb-3">
            <label for="email" class="form-label">Email</label>
            <input type="text" class="form-control" id="email" placeholder="{{ $user->email }}">
        </div>
        <form action="/user/profile/saved" method="POST">
            @csrf
            <div class="mb-3">
                <label for="telp" class="form-label">No telp</label>
                <input type="text" class="form-control" name="telp" id="telp" placeholder="+6212345678">
            </div>
            <div class="mb-3">
                <label for="alamat" class="form-label">Alamat</label>
                <input type="text" name="alamat" class="form-control" id="alamat" placeholder="alamat">
            </div>
            <label for="kota" class="form-label">Kota</label>
            <div class="input-group mb-3">
                <label class="input-group-text" for="inputGroupSelect01">Kota</label>
                <select name="kota" class="form-select" id="inputGroupSelect01">
                  <option selected"> Choose... </option>
                  @foreach ($kota as $item)
                  <option value="{{ $item->city_id }}">{{ $item->city_name }}</option>
                  @endforeach
                </select>
            </div>
            <div class="d-grid gap-2 d-md-flex justify-content-md-end">
                <button class="btn btn-primary me-md-2" name="store" type="submit">Submit</button>
              </div>
        </form>
    </div>
@endsection