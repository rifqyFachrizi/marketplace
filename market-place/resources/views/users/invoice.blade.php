@php
    use Illuminate\Support\Facades\DB;
    use App\Models\Produk;
@endphp

@extends('layouts.app')

@section('content')
<div class="container">
    <ul class="list-group">
        <li class="list-group-item active" aria-current="true">Identitas Pelanggan</li>
        <li class="list-group-item">Nama : {{ $user->name }}</li>
        <li class="list-group-item">Email : {{ $user->email }}</li>
        <li class="list-group-item">No Telp : </li>
        <li class="list-group-item">Alamat : </li>
        <li class="list-group-item">Kota : </li>
        <li class="list-group-item">Provinsi : </li>
      </ul>
    <table class="table">
        <thead>
            <tr>
        <th scope="col">No</th>
        <th scope="col">produk</th>
        <th scope="col">berat</th>
        <th scope="col">jumlah</th>
        <th scope="col">harga</th>
        <th scope="col">berat total</th>
        <th scope="col">harga total</th>
        <th></th>
    </tr>
</thead>
<tbody>
    @php    
        $i=1;

        // $arr = array_merge($datas,$jml);
        // dd($arr);

        $total_belanja = 0;
        $total_berat = 0;
        @endphp
        @foreach ($count as $item)
        @php
            $id_produk = $item->produk_id;
            // dump($item);
            $produk = Produk::find($id_produk);
            // dump($produk);
            $jmlh = $produk->detail->harga_produk*$item->jumlah;
            $total_belanja += $jmlh;
            $berat = $produk->berat*$item->jumlah;
            $total_berat += $berat;
            
            

        @endphp
        <tr>
            <th scope="row">{{ $i }}</th>
            <td>{{ $produk->nama_produk }}</td>
            <td>{{ $produk->stock }}</td>
            <td>{{ $item->jumlah }}</td>
            <td>Rp {{ number_format($produk->detail->harga_produk) }}</td>
            <td>{{ $berat }} Kg</td>
            <td>Rp {{ number_format($jmlh) }}</td>
            <td></td>
        </tr>
        @php
            $i++;
        @endphp
    @endforeach
    </tbody>
    <tfoot>
        <tr>
            <th colspan="5">Total Belanja</th>
            <th>{{ $total_berat }} Kg</th>
            <th>Rp {{ number_format($total_belanja) }}</th>
            
            
        </tr>
    </tfoot>
</table>
<div class="d-grid gap-2 d-md-flex justify-content-md-end">
    <a href="/home" class="mr-2 btn btn-secondary">Back</a>
    <a href="/order/store/{{ $id_user }}" class="btn btn-primary">Checkout</a>
</div>
</div>
@endsection