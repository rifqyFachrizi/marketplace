<?php

namespace App\Http\Controllers;

use App\Models\Cart;
use App\Models\Produk;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Models\User;
use GrahamCampbell\ResultType\Result;
use Illuminate\Support\Facades\Auth;

class OrderController extends Controller
{
    public function detail($id)
    {
        $detail = Produk::find($id);
        $str = $detail->detail->gambar;
        $pecah = explode('\\', $str);
        $img = end($pecah);
        // dd($img);
        return view('users.detail',[
            'data' => $detail,
            'img' => $img
        ]);
    }

    public function cartStore($id)
    {
        // cek dulu apakah barang/id_produk ada di cart? jika ada + jika tidak ada = 1
        $user = Auth::user()->id;

        $cart = DB::table('carts')
                ->where('produk_id',$id)
                ->where('user_id', $user)
                ->get()->all();
        // $cart harus disesuaikan dengan produk id dan user id
        // dd($cart);
        if($cart == []){
            $jml = 1;
            $cart = new Cart;
            
            $cart->produk_id = $id;
            $cart->user_id = $user;
            $cart->jumlah = $jml;
            $cart->save();
        } else {
            $count = $cart[0]->jumlah;
            // $cek = 'ada';
            // jika ada update jumlahnya saja
            $cart = DB::table('carts')
              ->where('produk_id', $id)
              ->where('user_id', $user)
              ->update(['jumlah' => $count+1]);
        }

        // dd($cart[0]->jumlah);

        

        return redirect('/order/cart/'. $user);
    }

    public function cartMinus($id_produk)
    {
        // mengurangi data pada database
        $user = Auth::user()->id;

        $cart = DB::table('carts')
                ->where('produk_id',$id_produk)
                ->where('user_id', $user)
                ->get()->all();
        $count = $cart[0]->jumlah;
        
        if($count == 1){
            // delet data dari database
            $cart = DB::table('carts')
                ->where('produk_id', $id_produk)
                ->where('user_id', $user)
                ->delete();
        }else {
            $cart = DB::table('carts')
                ->where('produk_id', $id_produk)
                ->where('user_id', $user)
                ->update(['jumlah' => $count-1]);
        }

        return redirect('/order/cart/'. $user);
        // dd($count);
    }

    public function cart($id_user)
    {
        // cost
        // cost
    $curl = curl_init();

    curl_setopt_array($curl, array(
    CURLOPT_URL => "https://api.rajaongkir.com/starter/cost",
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_ENCODING => "",
    CURLOPT_MAXREDIRS => 10,
    CURLOPT_TIMEOUT => 30,
    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
    CURLOPT_CUSTOMREQUEST => "POST",
    CURLOPT_POSTFIELDS => "origin=344&destination=&weight=&courier=",
    CURLOPT_HTTPHEADER => array(
        "content-type: application/x-www-form-urlencoded",
        "key: 8a3776dea95e31e1685d2feda624c6a5"
    ),
    ));

    $response = curl_exec($curl);
    $err = curl_error($curl);

    curl_close($curl);

    $result = json_decode($response);
            
    // $coba = $result->rajaongkir->results;
    // dd($coba);

        $user_id = $id_user;
        $produk = DB::table('carts')->where('user_id', $id_user)->get();
        $pecah_produk = collect($produk)->map(function ($item, $key){
            return $item->produk_id;
        }); 
        
        // dd($produk);
        return view('users.cart',[
            'count'=> $produk,
            'id_user' => $user_id
        ]);
    }

    public function invoice($id_user){
        // keluar cost ongkir beserta semua total harga pembelian
        $carts = DB::table('carts')->where('user_id', $id_user)->get();
        $userData = User::find($id_user);
        // dd($userData);

        return view('users.invoice',[
            'count' => $carts,
            'id_user'=> $id_user,
            'user' => $userData
        ]);
    }

    public function store($id_user){
        // masuk ke table pembelian, ambil semua yang di cart yg berdasar user id
        // dd($id_user);
        $order = Cart::where('user_id', $id_user)->get()->all();
        // dd($order);
        foreach($order as $item){
            // dump($item);
        } 
    }
}


// cart belum clear
