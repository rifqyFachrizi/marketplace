<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Detail;
use App\Models\Produk;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class AdminController extends Controller
{
    public function login(){
        return view('auth.loginAdmin');
    }

    public function verify(Request $request)
    {
        $name = $request->email;
        $pw = $request->password;

        if($name == 'admin@mail.com' && $pw == 'admin123'){
            return redirect('/admin/dashboard');
        } else {
            return redirect('/admin');
        }
    }

    public function dashboard()
    {
        return view('admin.dashboard');
    }

    public function show()
    {
        $data = Produk::all();
        return view('admin.show',[
            'datas' => $data
        ]);
    }
    public function create()
    {
        return view('admin.create');
    }
    public function store(Request $request)
    {
        $idProduk = Produk::all()->last();
        if($idProduk == true) {
        $id = $idProduk->id + 1;
        
        $request->file('gambar_produk')->move('image');

        $product = new Produk;
        $detail = new Detail;

        $product->nama_produk = $request->nama_produk;
        $product->stock = $request->stock_produk;
        $product->berat = $request->berat_produk;

        $detail->harga_produk = $request->harga_produk;
        $detail->deskripsi = $request->deskripsi;
        $detail->produk_id = $id;
        $detail->gambar = $request->gambar_produk;

        $detail->save();
        $product->save();

        } else {
        $id = 1;

        $request->file('gambar_produk')->move('image');

        $product = new Produk;
        $detail = new Detail;

        $product->nama_produk = $request->nama_produk;
        $product->stock = $request->stock_produk;
        $product->berat = $request->berat_produk;

        $detail->harga_produk = $request->harga_produk;
        $detail->produk_id = $id;
        $detail->deskripsi = $request->deskripsi;
        $detail->gambar = $request->gambar_produk;

        $detail->save();
        $product->save();


        }
      
        return redirect('/admin/create')->with('success','item saved');
    }


    public function edit($id)
    {
        $produk = Produk::find($id);
        // dd($produk->detail);
        $namaProduk = $produk->nama_produk;
        $stock = $produk->stock;
        $berat = $produk->berat;
        $harga = $produk->detail->harga_produk;
        $deskripsi = $produk->detail->deskripsi;
        
        $str = $produk->detail->gambar;
        $strPecah = explode('\\', $str);
        $img = $strPecah[3];

        // dd($img);

        return view('admin.edit',[
            'id' => $id,
            'namaProduk' => $namaProduk,
            'stock' => $stock,
            'berat' => $berat,
            'harga' => $harga,
            'deskripsi' => $deskripsi,
            'img' => $img
        ]);
    }

    public function update(Request $request ,$id)
    {
        // dd($request);

        $request->file('gambar_produk')->move('image');
        // dd($request);
        $update = Produk::find($id);
        $detail = Detail::find($id);
        // dd($detail);
        $update->nama_produk = $request->nama_produk;
        $update->stock = $request->stock_produk;
        $update->berat = $request->berat_produk;
        $detail->harga_produk = $request->harga_produk;
        $detail->deskripsi = $request->deskripsi;
        $detail->gambar = $request->gambar_produk;
        $update->update();
        $detail->update(); //solved

        return redirect('/admin/edit/'.$id )->with('success','item saved');
    }


    public function destroy($id)
    {
        Produk::find($id)->delete();
        Detail::find($id)->delete();

        return redirect('/admin/products');
    }

    public function showCostumers()
    {
        $user = User::all();
        // dd($user);
        return view('admin.costumer', [
            'users' => $user
        ]);
    }
}
