<?php

namespace App\Http\Controllers;

use Exception;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use App\Providers\RouteServiceProvider;
use Laravel\Socialite\Facades\Socialite;

class GoogleController extends Controller
{

    protected $redirectTo = RouteServiceProvider::HOME;



    public function redirectToGoogle(){
        return Socialite::driver('google')->redirect();
    }

    public function handleGoogleCallback(){
        try {
            $user = Socialite::driver('google')->user();
            $find = User::where('google_id', $user->id)->first();

            if($find){
                Auth::login($find);
      
                return redirect($this->redirectTo);
            }else {
                $arrUser = User::create([
                    'name' => $user->name,
                    'email' => $user->email,
                    'google_id' => $user->id 
                ]);
    
                Auth::login($arrUser, true);
                return redirect($this->redirectTo);
            }

        } catch (Exception $e) {
            dd($e->getMessage());
        }
 
 
    }
}
