<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Models\Profile;
use App\Models\User;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    public function profile($id_user){
        // provinsi
    $curl = curl_init();

    curl_setopt_array($curl, array(
    CURLOPT_URL => "https://api.rajaongkir.com/starter/province",
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_ENCODING => "",
    CURLOPT_MAXREDIRS => 10,
    CURLOPT_TIMEOUT => 30,
    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
    CURLOPT_CUSTOMREQUEST => "GET",
    CURLOPT_HTTPHEADER => array(
        "key: 8a3776dea95e31e1685d2feda624c6a5"
    ),
    ));

    $response = curl_exec($curl);
    $err = curl_error($curl);

    curl_close($curl);

    $datas = json_decode($response);
    $hasil = $datas->rajaongkir->results;
    // dd($hasil);
        // kota
        $curl = curl_init();

        curl_setopt_array($curl, array(
        CURLOPT_URL => "https://api.rajaongkir.com/starter/city",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "GET",
        CURLOPT_HTTPHEADER => array(
            "key: 8a3776dea95e31e1685d2feda624c6a5"
        ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        $result = json_decode($response);
        
        $coba = $result->rajaongkir->results;
        // dd($coba);

        $user = User::all()->find($id_user);
        $user_profile = Profile::where('user_id', $id_user)->get()->all();

        // dd($kota);
        if($user_profile == []){
            return view('users.profileFrom',[
                "kota" => $coba,
                "user" => $user
            ]);
        } else {
            $kota_id = $user_profile[0]->kota;
            $kota = $coba[$kota_id-1];
        return view('users.profile',[
            "data" => $user_profile[0],
            "user" => $user,
            "kota" => $coba,
            "provinsi" => $hasil,
            "nama_kota" => $kota,
        ]);
    }


    }

    public function saved(Request $request){
        //  dd($request);
                         // kota
        $curl = curl_init();

        curl_setopt_array($curl, array(
        CURLOPT_URL => "https://api.rajaongkir.com/starter/city",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "GET",
        CURLOPT_HTTPHEADER => array(
            "key: 8a3776dea95e31e1685d2feda624c6a5"
        ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        $result = json_decode($response);
        $coba = $result->rajaongkir->results;

        $id_kota = $request->kota-1;
        $prov = $coba[$id_kota]->province;
        // dd($prov);
    
        $id_user = Auth::user()->id;
        $store = new Profile();

        // dd($store);
        $store->user_id = $id_user;
        $store->telp = $request->telp;
        $store->alamat = $request->alamat;
        $store->kota = $request->kota;
        $store->prvinsi = $prov;
        $store->save();

        return redirect('user/profile/'.$id_user);
    }


    public function storeProfile(Request $request, $id_user){
        // dd($request);
                // kota
        $curl = curl_init();

        curl_setopt_array($curl, array(
        CURLOPT_URL => "https://api.rajaongkir.com/starter/city",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "GET",
        CURLOPT_HTTPHEADER => array(
            "key: 8a3776dea95e31e1685d2feda624c6a5"
        ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        $result = json_decode($response);
        $coba = $result->rajaongkir->results;

        $id_kota = $request->kota-1;
        $prov = $coba[$id_kota]->province;
        // dd($prov);
    
        $id_user = Auth::user()->id;
        $store = Profile::where('user_id', $id_user)->get()->all()[0];

        // dd($store);
        $store->user_id = $id_user;
        $store->telp = $request->telp;
        $store->alamat = $request->alamat;
        $store->kota = $request->kota;
        $store->prvinsi = $prov;
        $store->update();

        return redirect('user/profile/'.$id_user);
    }
}
