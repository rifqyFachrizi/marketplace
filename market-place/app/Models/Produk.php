<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Produk extends Model
{
    use HasFactory;

    protected $fillable = ['nama_produk','stock','berat'];

    public function detail()
    {
        return $this->hasOne(Detail::class);
    }

    public function cart()
    {
        return $this->belongsTo(Cart::class);
    }
}
