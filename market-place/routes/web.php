<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\GoogleController;
use App\Http\Controllers\AdminController;
use App\Http\Controllers\OrderController;
use App\Http\Controllers\UserController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('/admin', [AdminController::class, 'login']);
Route::get('/admin/dashboard', [AdminController::class, 'dashboard']);
Route::post('/admin/verify', [AdminController::class, 'verify']);
Route::get('/admin/products', [AdminController::class, 'show']);
Route::get('/admin/create', [AdminController::class, 'create']);
Route::post('/admin/store', [AdminController::class, 'store']);
Route::get('/admin/edit/{id}', [AdminController::class, 'edit']);
Route::put('/admin/edit/update/{id}', [AdminController::class, 'update']);
Route::get('/admin/delete/{id}', [AdminController::class, 'destroy']);



// costumers
Route::get('/admin/costumers/', [AdminController::class, 'showCostumers']);

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::get('/auth/google', [GoogleController::class, 'redirectToGoogle']);
Route::get('/auth/google/callback', [GoogleController::class, 'handleGoogleCallback']);

// user birokrasi
Route::get('/user/profile/{id_user}', [UserController::class, 'profile']);
Route::post('/user/profile/saved', [UserController::class, 'saved']);
Route::put('/user/profile/store/{id}', [UserController::class, 'storeProfile']);

// user order routes
Route::get('/order/detail/{id}', [OrderController::class, 'detail']);
Route::get('/order/cart/store/{id}', [OrderController::class, 'cartStore']);
Route::get('/order/cart/minus/{id_produk}', [OrderController::class, 'cartMinus']);
Route::get('/order/cart/{id_user}', [OrderController::class, 'cart']);
Route::get('/order/invoice/{id_user}', [OrderController::class, 'invoice']);
Route::get('order/store/{id_user}', [OrderController::class, 'store']);